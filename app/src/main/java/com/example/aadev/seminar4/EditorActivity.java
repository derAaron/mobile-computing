package com.example.aadev.seminar4;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class EditorActivity extends AppCompatActivity {
    public final static String MOVIE_PARCELABLE = "MOVIE_PARCELABLE";
    private EditText title, year, fname, lname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        title = findViewById(R.id.editor_title);
        year = findViewById(R.id.editor_year);
        fname = findViewById(R.id.editor_fname);
        lname = findViewById(R.id.editor_lname);


        Bundle args = getIntent().getExtras();
        if(args!=null){
            Movie m = args.getParcelable(MOVIE_PARCELABLE);
            title.setText(m.getTitle());
            year.setText(""+m.getYear());
            fname.setText(m.getDirector().getFirstName());
            lname.setText(m.getDirector().getLastName());
        }
    }

    public void onOkClick(View v){
        Movie m = new Movie(title.getText().toString(),
                Integer.valueOf(year.getText().toString()),
                new Person(fname.getText().toString(), lname.getText().toString()));
        Intent i = new Intent();
        i.putExtra(EditorActivity.MOVIE_PARCELABLE, m);
        setResult(MainActivity.RESULT_OK, i);
        finish();
    }

    public void onCancelClick(View v){
        finish();
    }
}
