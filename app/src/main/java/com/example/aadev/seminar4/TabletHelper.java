package com.example.aadev.seminar4;

import android.content.Context;


public class TabletHelper {

    public static boolean isTablet(Context context) {
        return context.getResources().getBoolean(R.bool.isTablet);
    }
}