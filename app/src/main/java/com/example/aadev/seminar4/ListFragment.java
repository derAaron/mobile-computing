package com.example.aadev.seminar4;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aadev on 13.12.17.
 */

interface ClickHandler {
    void onItemClick(View v, Movie m, int position);
}

public class ListFragment extends Fragment implements ClickHandler {

    private List<Movie> movies = new ArrayList<>();
    private MoviesRecyclerAdapter mra;
    private MovieListAdapter la;

    public void loadData(){

        for(int i = 0; i < 10; i++) {
            movies.add(new Movie("Titel " + i, i+1980, new Person("bla",i+"")));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);


        loadData();

        final ListView mylist = view.findViewById(R.id.mylistview);
        la = new MovieListAdapter(getContext(), this.movies);
        mylist.setAdapter(la);

        mylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(view.getContext(), "Nummer " + i, Toast.LENGTH_LONG).show();
            }
        });

        mylist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                movies.remove(i);
                la.notifyDataSetChanged();
                Log.v("nummer", String.valueOf(i));
                return true;
            }
        });

        mra = new MoviesRecyclerAdapter(movies, this);
        RecyclerView mrv = view.findViewById(R.id.recyclerView);
        mrv.setLayoutManager(new LinearLayoutManager(getContext()));
        mrv.setAdapter(mra);

        return view;
    }


    @Override
    public void onItemClick(View v, Movie m, int position) {

        if(TabletHelper.isTablet(getContext())) {


        } else {
            Intent i = new Intent(getActivity(), EditorActivity.class);
            i.putExtra(EditorActivity.MOVIE_PARCELABLE, m);
            this.startActivityForResult(i, position);
        }
    }
}
