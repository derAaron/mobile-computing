package com.example.aadev.seminar4;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by aadev on 29.11.17.
 */

public class MoviesRecyclerAdapter extends Adapter<MoviesRecyclerAdapter.ViewHolder> {
    private List<Movie> movies;
    Fragment fragment;

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year;
        public LinearLayout linearlayout;


        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            year = itemView.findViewById(R.id.year);
            linearlayout = itemView.findViewById(R.id.item_view);
        }
    }

    MoviesRecyclerAdapter(List<Movie> m, Fragment a) {
        this.movies = m;
        this.fragment = a;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Movie m = movies.get(position);
        holder.title.setText(m.getTitle());
        holder.year.setText(m.getYear()+"");
        holder.linearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ClickHandler) fragment).onItemClick(view, m, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }
}
