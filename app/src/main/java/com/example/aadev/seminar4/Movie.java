package com.example.aadev.seminar4;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aadev on 15.11.17.
 */

public class Movie implements Parcelable {
    String title;

    protected Movie(Parcel in) {
        title = in.readString();
        director = in.readParcelable(Person.class.getClassLoader());
        year = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeParcelable(director, flags);
        dest.writeInt(year);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public Person getDirector() {
        return director;
    }

    public void setDirector(Person director) {
        this.director = director;
    }

    Person director;

    public Movie(String title, int year, Person director) {
        this.title = title;
        this.year = year;
        this.director = director;
    }

    int year;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
