package com.example.aadev.seminar4;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by aadev on 13.12.17.
 */

public class EditFragment extends Fragment {

    public final static String MOVIE_PARCELABLE = "MOVIE_PARCELABLE";
    private EditText title, year, fname, lname;
    private Movie myMovie;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit, container, false);

        title = view.findViewById(R.id.editor_title);
        year = view.findViewById(R.id.editor_year);
        fname = view.findViewById(R.id.editor_fname);
        lname = view.findViewById(R.id.editor_lname);


        Bundle args = getArguments();
        if(args!=null){
            myMovie = args.getParcelable(MOVIE_PARCELABLE);
            setFields(myMovie);
        }

        view.findViewById(R.id.editor_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Movie m = new Movie(title.getText().toString(),
                        Integer.valueOf(year.getText().toString()),
                        new Person(fname.getText().toString(), lname.getText().toString()));
                Intent i = new Intent();
                i.putExtra(EditorActivity.MOVIE_PARCELABLE, m);
//                setResult(MainActivity.RESULT_OK, i);
//                finish();
            }
        });

        view.findViewById(R.id.editor_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getResources().getBoolean(R.bool.isTablet)) {
                    setFields(myMovie);
                } else {
                    getActivity().finish();
                }
            }
        });

        return view;
    }

    private void setFields(Movie m) {
        title.setText(m.getTitle());
        year.setText(""+ m.getYear());
        fname.setText(m.getDirector().getFirstName());
        lname.setText(m.getDirector().getLastName());
    }
}
