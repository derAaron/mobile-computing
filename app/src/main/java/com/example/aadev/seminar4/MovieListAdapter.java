package com.example.aadev.seminar4;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by aadev on 15.11.17.
 */

public class MovieListAdapter extends BaseAdapter {
    List<Movie> movies;
    Context c;

    public MovieListAdapter(Context c, List<Movie> movies) {

        this.c = c;
        this.movies = movies;
    }

    @Override
    public int getCount() {
        return this.movies.size();
    }

    @Override
    public Object getItem(int i) {
        return this.movies.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(c).inflate(R.layout.movie_list_item, viewGroup, false);
        Movie m = movies.get(i);
        ((TextView) v.findViewById(R.id.title)).setText(m.title);
        ((TextView) v.findViewById(R.id.year)).setText(String.valueOf(m.year));
        return v;
    }
}
